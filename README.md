# 项目简介

本项目是用了VUE2仿制了旅游网，涉及到的有技术有以下内容：

1)轮播图片以及轮播按钮的实现，使用的是 swiper 6.2.0 


2）城市选择，使用vuex共享数据,实现了多个组件之间的数据共享


3）城市模糊搜索和字母拖动搜索


## 项目图片展示

![输入图片说明](https://www.hualigs.cn/image/6060a15b9e25c.jpg "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0328/233340_94943978_5638761.gif "2.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0328/233351_8e2ea074_5638761.gif "3.gif")


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```


