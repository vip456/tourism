let defaultCity = '上海'
try {
  if (localStorage.city) {
    defaultCity = localStorage.city
  }
} catch (e) {} /* try catch是为了当用户关闭本地缓存或者使用隐身模式避免报错 */

export default {
  city: defaultCity
}
